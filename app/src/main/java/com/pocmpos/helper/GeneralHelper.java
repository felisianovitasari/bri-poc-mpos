package com.pocmpos.helper;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.pocmpos.R;

import java.util.Arrays;

/**
 * Created by ~FNS on 13/04/21
 */
public class GeneralHelper {
    public static int dpToPx(Context c, int dp) {
        Resources r = c.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    public static String showPINPad(Context context, TextView textView, String titleNumber,  int idField) {
        final String[] res = new String[1];

        final int SPAN_COUNT = 3;
        final Dialog d = new BottomSheetDialog(context);
        d.setContentView(R.layout.bottomsheet_number);
        final TextView title = (TextView) d.findViewById(R.id.title);
        title.setText(titleNumber);
        final RecyclerView rvNumber = (RecyclerView) d.findViewById(R.id.rvPinPad);
        final String[] listNumber = context.getResources().getStringArray(R.array.angka);
        final PINPadView pinPadView = (PINPadView) d.findViewById(R.id.pinPad);
        final CardView cvCancel = (CardView) d.findViewById(R.id.cvPINCancel);
        final CardView cvNum0 = (CardView) d.findViewById(R.id.cvNum0);
        final CardView cvOk = (CardView) d.findViewById(R.id.cvPINOk);
        final View vOk = d.findViewById(R.id.rvPINOk);
        final TextView tvTexCvCancel = (TextView) d.findViewById(R.id.tvTextCvCancel);
        cvOk.setEnabled(false);
        vOk.setBackgroundColor(ContextCompat.getColor(context,R.color.color_pinpad_disable));

        PINNumberAdapter numberAdapter = new PINNumberAdapter(context, listNumber, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tvText =  (TextView) v.findViewById(R.id.textPINNum);
                String num = tvText.getText().toString();
                if(pinPadView.isEmpty()) tvTexCvCancel.setText("Hapus");
                pinPadView.addPIN(num);
                if(pinPadView.isFull()) {
                    cvOk.setEnabled(true);
                    vOk.setBackgroundColor(ContextCompat.getColor(context,R.color.color_pinpad_ok));
                }
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(context, SPAN_COUNT);
        rvNumber.setLayoutManager(mLayoutManager);
        rvNumber.addItemDecoration(new GridSpacingItemDecoration(SPAN_COUNT, GeneralHelper.dpToPx(context, 1), true));
        rvNumber.setItemAnimator(new DefaultItemAnimator());
        rvNumber.setAdapter(numberAdapter);


        cvNum0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pinPadView.isEmpty()) tvTexCvCancel.setText("Hapus");
                pinPadView.addPIN("0");
                if(pinPadView.isFull()) {
                    cvOk.setEnabled(true);
                    vOk.setBackgroundColor(ContextCompat.getColor(context,R.color.color_pinpad_ok));
                }
            }
        });
        cvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pinPadView.isEmpty()){
                    d.dismiss();
                }
                else {
                    pinPadView.resetPIN();
                    cvOk.setEnabled(false);
                    vOk.setBackgroundColor(ContextCompat.getColor(context,R.color.color_pinpad_disable));
                    if(pinPadView.isEmpty()) tvTexCvCancel.setText("Batal");
                }
            }
        });
        cvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pinPadView.isFull()){
                    textView.setText(pinPadView.getPinValue());
                    res[0] = pinPadView.getPinValue();
                    d.dismiss();
                }
            }
        });
        d.setCancelable(true);
        d.show();
        return Arrays.toString(res);
    }
    public static byte[] getBRILogoByte(Context mContext) {
        byte[] command = "".getBytes();
        Bitmap bmp = null, bmp1 = null;
        try {
            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.inMutable = true;
            bmp = BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.bank_bri_logo_termal, opt);
//                int width = mm2pixels(420, mContext);
//                int height = mm2pixels(120, mContext);
            bmp1 = Bitmap.createScaledBitmap(bmp, 400, 115, true);
            try{
                if(!bmp.isRecycled())bmp.recycle();
            }
            catch (Exception e)
            {}

            if(bmp1!=null){
                command = ThermalUtils.decodeBitmap(bmp1);
            }
        } catch (Exception e) {
        }
        return command;
    }
}
