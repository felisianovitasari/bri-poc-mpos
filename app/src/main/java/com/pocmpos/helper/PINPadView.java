package com.pocmpos.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.pocmpos.R;

public class PINPadView extends View {


    private float strokeWidth = 4;
    private float circleSize = 50;
    private int maxPinLength = 6;
    private int currentPinLength = 0;
    private String pinValue = "";

    private float circlePaddingTop = 18f;
    private float circlePaddingBottom = 18f;
    private float circlePaddingLeft = 18f;
    private float circlePaddingRight = 18f;


    private int colorEmpty = Color.GRAY;
    private int colorFilled = Color.GRAY;

    private Paint backgroundPaint;
    private Paint foregroundPaint;

    public PINPadView(Context context) {
        super(context);
    }

    public PINPadView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.PINPadView,
                0, 0);

        //Reading values from the XML layout
        try {
            circleSize = typedArray.getFloat(R.styleable.PINPadView_circleSize, circleSize);
            circlePaddingTop = typedArray.getFloat(R.styleable.PINPadView_circlePaddingTop, circlePaddingTop);
            circlePaddingBottom = typedArray.getFloat(R.styleable.PINPadView_circlePaddingBottom, circlePaddingBottom);
            circlePaddingLeft = typedArray.getFloat(R.styleable.PINPadView_circlePaddingLeft, circlePaddingLeft);
            circlePaddingRight = typedArray.getFloat(R.styleable.PINPadView_circlePaddingRight, circlePaddingRight);
            maxPinLength = typedArray.getInt(R.styleable.PINPadView_maxPinLength, maxPinLength);
            currentPinLength = typedArray.getInt(R.styleable.PINPadView_currentPinLength, currentPinLength);
            colorEmpty = typedArray.getInt(R.styleable.PINPadView_colorEmpty, colorEmpty);
            colorFilled = typedArray.getInt(R.styleable.PINPadView_colorFilled, colorFilled);
        } finally {
            typedArray.recycle();
        }

        backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        backgroundPaint.setColor(adjustAlpha(colorEmpty, 0.3f));
        backgroundPaint.setStrokeWidth(strokeWidth);

        foregroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        foregroundPaint.setColor(colorFilled);
        foregroundPaint.setStrokeWidth(strokeWidth);
    }

    private int adjustAlpha(int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(getViewWidth(), getViewHeight());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for(int i = 1; i <= maxPinLength; i++){
            canvas.drawCircle(getCirclePosX(i), getCirclePosY(), circleSize, backgroundPaint);
        }

        for(int i = 1; i <= currentPinLength; i++){
            canvas.drawCircle(getCirclePosX(i), getCirclePosY(), circleSize, foregroundPaint);
        }
    }

    private float getCirclePosX(int i){
        return (i * circleSize * 2 - circleSize) + i * (circlePaddingLeft + circlePaddingRight) - circlePaddingRight;
    }

    private float getCirclePosY(){
        return circleSize + circlePaddingTop;
    }

    private int getViewWidth(){
        return (int) (circleSize * maxPinLength * 2) + maxPinLength * (int)(circlePaddingLeft + circlePaddingRight);
    }

    private int getViewHeight(){
        return  (int) (circleSize * 2 + circlePaddingTop + circlePaddingBottom);
    }

    public void addPIN(String num){
        if(pinValue.length() >= maxPinLength) return;

        pinValue += num;
        currentPinLength = pinValue.length();
        invalidate();
    }

    public void resetPIN(){
        pinValue = "";
        currentPinLength = pinValue.length();
        invalidate();
    }

    public String getPinValue() {
        return pinValue;
    }

    public boolean isEmpty(){
        return pinValue.isEmpty();
    }


    public boolean isFull(){
        return pinValue.length() == maxPinLength;
    }
}
