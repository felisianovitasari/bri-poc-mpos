package com.pocmpos.helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.pocmpos.R;

/**
 * Created by ivanharsono on 3/14/17.
 */

public class PINNumberAdapter extends RecyclerView.Adapter<PINNumberAdapter.PINNumberViewHolder> {
    private Context mContext;
    private String[] listNumber;
    private View.OnClickListener clickListener;

    public class PINNumberViewHolder extends RecyclerView.ViewHolder {
        public View view;
        public CardView cvPINNumber;
        public TextView textPINNum;

        public PINNumberViewHolder(View view) {
            super(view);

            this.view = view;
            cvPINNumber = (CardView) view.findViewById(R.id.cvPINNumber);
            textPINNum = (TextView) view.findViewById(R.id.textPINNum);
        }
    }

    public PINNumberAdapter(Context mContext, String[] listNumber, View.OnClickListener clickListener) {
        this.mContext = mContext;
        this.listNumber = listNumber;
        this.clickListener = clickListener;
    }

    @Override
    public PINNumberViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_number, parent, false);

        return new PINNumberViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PINNumberViewHolder holder, int position) {
        String text = listNumber[position];

        holder.textPINNum.setText(text);
        holder.cvPINNumber.setOnClickListener(this.clickListener);
    }

    @Override
    public int getItemCount() {
        return listNumber.length;
    }
}
