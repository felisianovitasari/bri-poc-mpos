package com.pocmpos.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.pocmpos.iinterface.IsdkInterface;
import com.pocmpos.R;

/**
 * Created by ~FNS on 13/04/21
 */
public class SplashScreenActivity extends AppCompatActivity implements IsdkInterface {

    //implement this method to connect on your sdk
    @Override
    public void connectSDK() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        connectSDK();

        new Handler().postDelayed(new Runnable(){
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(SplashScreenActivity.this, MenuActivity.class);
                mainIntent.putExtra("slide",true);
                startActivity(mainIntent);
                finish();
            }
        }, 1000);
    }
}
