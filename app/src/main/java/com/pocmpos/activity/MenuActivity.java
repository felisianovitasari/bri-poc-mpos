package com.pocmpos.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;


import com.pocmpos.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.btn_magcard)
    Button btnMagCard;
    @BindView(R.id.btn_nfc)
    Button btnNfc;
    @BindView(R.id.btn_print)
    Button btnPrint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);

        btnMagCard.setOnClickListener(this);
        btnNfc.setOnClickListener(this);
        btnPrint.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_magcard:
                ReadCardActivity.launchIntent(this);
                break;
            case R.id.btn_nfc:
                ReadNFCCardActivity.launchIntent(this);
                break;
            case R.id.btn_print:
                ResultActivity.launchIntent(this);
                break;
        }
    }
}