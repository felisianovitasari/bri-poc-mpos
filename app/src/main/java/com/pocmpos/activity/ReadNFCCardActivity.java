package com.pocmpos.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.pocmpos.R;
import com.pocmpos.iinterface.IcardInterface;

public class ReadNFCCardActivity extends AppCompatActivity implements IcardInterface {

    public static void launchIntent(Activity caller){
        Intent intent = new Intent(caller,ReadCardActivity.class);
        caller.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_nfc_card);
        checkCard();
    }


    //implement this method to get NFC card value
    @Override
    public void checkCard() {
        //return the card data
    }
}