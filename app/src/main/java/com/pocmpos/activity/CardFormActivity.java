package com.pocmpos.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.pocmpos.helper.GeneralHelper;
import com.pocmpos.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ~FNS on 13/04/21
 */
public class CardFormActivity extends AppCompatActivity implements View.OnClickListener {
    static Bundle mBundle;
    @BindView(R.id.etCardNumber)
    AutoCompleteTextView edCardNumber;
    @BindView(R.id.etExpiryDate)
    AutoCompleteTextView edExpDate;
    @BindView(R.id.edPin)
    EditText edPin;
    @BindView(R.id.etAmount)
    EditText edAmount;
    @BindView(R.id.submit)
    Button btnSubmit;

    String track2;
    public static void launchIntent(Activity caller, Bundle bundle){
        Intent intent = new Intent(caller,CardFormActivity.class);
        mBundle = bundle;
        caller.startActivityForResult(intent, 111);
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_form);
        ButterKnife.bind(this);

        if(mBundle != null) {
            //key can be changed
            //can be modified on IC Card case
            track2 = mBundle.getString("TRACK2");

            String[] track2Separated = track2.split("\u003d");
            edCardNumber.setText(track2Separated[0]);
            edExpDate.setText(track2Separated[1].substring(0,4));
        }
        edPin.setVisibility(View.GONE);
        btnSubmit.setEnabled(true);
        btnSubmit.setOnClickListener(this);
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.submit :
                ReviewActivity.launchIntent(this, edCardNumber.getText().toString(), edExpDate.getText().toString(), edAmount.getText().toString());
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
    }
}
