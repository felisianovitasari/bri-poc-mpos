package com.pocmpos.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.github.clans.fab.FloatingActionButton;
import com.pocmpos.helper.GeneralHelper;
import com.pocmpos.R;
import com.pocmpos.iinterface.IPrintInterface;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResultActivity extends AppCompatActivity implements View.OnClickListener, IPrintInterface {
    @BindView(R.id.fabStruk)
    FloatingActionButton fab;
    @BindView(R.id.receipt)
    TextView tvReceipt;
    String receipt;
    byte[] logoThermal;
    public static void launchIntent(Activity caller){
        Intent intent = new Intent(caller,ResultActivity.class);
        caller.startActivityForResult(intent,113);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        ButterKnife.bind(this);
        receipt = getResources().getString(R.string.receipt);
        fab.setOnClickListener(this);

        logoThermal = GeneralHelper.getBRILogoByte(this);
        setResult(RESULT_OK);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fabStruk:
                printReceipt(receipt,logoThermal);
                break;
        }
    }

    //implement this method to print the receipt
    @Override
    public void printReceipt(String msg, byte[] logoThermal) {
    }
}