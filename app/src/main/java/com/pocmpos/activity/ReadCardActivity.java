package com.pocmpos.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.pocmpos.iinterface.IcardInterface;
import com.pocmpos.R;

/**
 * Created by ~FNS on 13/04/21
 */
public class ReadCardActivity extends AppCompatActivity implements IcardInterface {

    public static void launchIntent(Activity caller){
        Intent intent = new Intent(caller,ReadCardActivity.class);
        caller.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_card);
        checkCard();
    }


    // implement this method to get card data (Magstripe and also IC)
    // call method handleResult to parse the data (onSuccess) (bundle should contain TRACK1, TRACK2, etc)
    // if chip card detected, then should return the bit55 data etc
    @Override
    public void checkCard() {


        /*onSuccess(bundle){
            handleResult(bundle);
        }*/
    }

    public void handleResult(Bundle bundle){
        CardFormActivity.launchIntent(this,bundle);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
    }
}
