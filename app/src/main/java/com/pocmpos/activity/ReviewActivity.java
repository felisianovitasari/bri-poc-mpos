package com.pocmpos.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.pocmpos.R;
import com.pocmpos.helper.GeneralHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ~FNS on 14/04/21
 */
public class ReviewActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.etCardNumber)
    AutoCompleteTextView edCardNumber;
    @BindView(R.id.etExpiryDate)
    AutoCompleteTextView edExpDate;
    @BindView(R.id.edPin)
    EditText edPin;
    @BindView(R.id.etAmount)
    EditText edAmount;
    @BindView(R.id.submit)
    Button btnSubmit;

    static String mCardNo = "", mExpDate = "", mAmount = "";

    public static void launchIntent(Activity caller, String cardNo, String expDate, String amount){
        Intent intent = new Intent(caller,ReviewActivity.class);
        mCardNo = cardNo;
        mExpDate = expDate;
        mAmount = amount;
        caller.startActivityForResult(intent, 112);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_form);
        ButterKnife.bind(this);
        edCardNumber.setText(mCardNo);
        edExpDate.setText(mExpDate);
        edAmount.setText(mAmount);
        edCardNumber.setEnabled(false);
        edAmount.setEnabled(false);
        edPin.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edPin:
                String pin = GeneralHelper.showPINPad(this, edPin, "Input PIN", 0);
                if (pin != null
                        || !pin.equals("")) {
                    doneInputPIN(pin);
                }
                break;
            case R.id.submit :
                ResultActivity.launchIntent(this);
                break;
        }
    }

    protected void doneInputPIN(String pin) {
        btnSubmit.setEnabled(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
    }
}
