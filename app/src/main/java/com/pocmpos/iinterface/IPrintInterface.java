package com.pocmpos.iinterface;

public interface IPrintInterface {
    void printReceipt(String msg, byte[] logoThermal);
}
